
## How to run:

* run without building a jar:

```
cd server
./gradlew bootRun
```

* compile and run jar

```
cd server
./gradlew build
java -jar build/libs/gs-messaging-stomp-websocket-0.1.0.jar
```


## Run with Docker
* build the image
```
sudo docker build -t q4bserver .
```

* run a container
```
sudo docker run -it -p 8080:8080 q4bserver
```



