package service;

import org.springframework.web.bind.annotation.RestController;

import sb3.modifiedParser.Sb3Json2AST;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class AnalysisController {
    
    @CrossOrigin
    @PostMapping("/analyze")
    public String analyze(@RequestBody String jsonstr) {
    	String msg = "{}";
    	try {
    		msg = new Sb3Json2AST().analyze(jsonstr);
    	} catch (Exception e) {
            e.printStackTrace(System.out);
    		msg = "{\"error\":\""+e.getClass()+"\"}";
    		if(e.getMessage() == "Unknown block type") {
    			msg = "{\"error\":\""+e.getMessage()+"\"}";
    		}
    	}
    	return msg;
    }

}