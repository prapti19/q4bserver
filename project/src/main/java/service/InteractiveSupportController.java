package service;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

/*
 * handling client-server communication via websocket
 */

@Controller
public class InteractiveSupportController {
	@MessageMapping("/request") // message destination
	@SendToUser // for peer-to-peer communication
	public ServerMsg clientRequestHandler(ClientMsg clientMsg) throws Exception {
		System.out.println(clientMsg);
		return new ServerMsg("Server msg via websocket");
	}
}
