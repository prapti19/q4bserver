package service;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
    	config.setApplicationDestinationPrefixes("/app");
    	/*
    	 * follow the convention of /user/queue 
    	 */
    	config.setUserDestinationPrefix("/user");
    	config.enableSimpleBroker("/queue");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
    	/*
    	 * The end point for the initial HTTP connection required by WebSocket protocol to
    	 * to ensure the browser's "same-origin" security model remains in effect.
    	 * Then the connection will be upgraded to WebSocket over TCP.
    	 * 
    	 * setAllowedOrigins("*") to allow cross origin requests
    	 */
    	// registry.addEndpoint("/service-endpoint").setAllowedOrigins("*").withSockJS();
    	// guide to remove sockjs https://github.com/nielsutrecht/spring-boot-websocket-client
    	
        registry.addEndpoint("/service-endpoint").setAllowedOrigins("*");
    }

}
